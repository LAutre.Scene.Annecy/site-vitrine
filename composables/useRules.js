import { useI18n } from '#imports';

export default () => {
  const { t } = useI18n();

  return {
    required: (val) => {
      if (!val || val.length < 1) {
        return t('validation.rules.required');
      }

      return false;
    },

    max: (val, length) => {
      if (val && val.length > length) {
        return t('validation.rules.max');
      }

      return false;
    },

    email: (val) => {
      if (
        !val.match(
          /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        )
      ) {
        return t('validation.rules.email');
      }

      return false;
    },
  };
};

import { ref } from 'vue';

export default function (form, rules) {
  const errors = ref({});

  const validate = (field) => {
    delete errors.value[field];

    if (rules[field] && rules[field].length) {
      for (const rule of rules[field]) {
        const result = rule(form.value[field]);

        if (result) {
          errors.value[field] = result;

          return;
        }
      }
    }
  };

  return { validate, errors };
}

import { defineNuxtConfig } from 'nuxt/config';

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  css: ['~/assets/css/main.css'],

  devtools: { enabled: true },

  modules: [
    // https://nuxt.com/modules/nuxt3-leaflet
    '@nuxtjs/leaflet',

    // https://github.com/nuxt-modules/eslint#readme
    '@nuxtjs/eslint-module',

    // https://i18n.nuxtjs.org
    [
      '@nuxtjs/i18n',
      {
        langDir: './locales/',
        lazy: true,
        locales: [
          { code: 'fr', file: 'fr.json', iso: 'fr-FR' },
          { code: 'en', file: 'en.json', iso: 'en-US' },
        ],
        defaultLocale: 'fr',
        strategy: 'prefix_and_default',
      },
    ],

    // https://nuxt.com/modules/nuxt-mail
    [
      'nuxt-mail',
      {
        message: {
          to: process.env.MAIL_TO,
        },
        smtp: {
          host: process.env.MAIL_HOST,
          port: process.env.MAIL_PORT || 587,
          auth: {
            user: process.env.MAIL_USER,
            pass: process.env.MAIL_PASS,
          },
        },
      },
    ],

    // https://color-mode.nuxtjs.org/
    [
      '@nuxtjs/color-mode',
      {
        preference: 'system',
        fallback: 'light',
        hid: 'nuxt-color-mode-script',
        globalName: '__NUXT_COLOR_MODE__',
        componentName: 'ColorScheme',
        classPrefix: '',
        classSuffix: '-mode',
        storage: 'localStorage',
        storageKey: 'nuxt-color-mode',
      },
    ],
  ],

  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },

  routeRules: {},

  runtimeConfig: {
    public: {
      mailFrom: process.env.MAIL_FROM,
    },
  },
});

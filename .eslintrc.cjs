module.exports = {
  root: true,
  env: {
    browser: true,
    es2021: true,
    node: true,
  },
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
    extraFileExtensions: ['.vue'],
  },
  extends: ['@nuxt/eslint-config', 'plugin:prettier/recommended'],
  ignorePatterns: ['.nuxt/**', 'dist/**', '.output/**'],
  plugins: ['vue'],
};

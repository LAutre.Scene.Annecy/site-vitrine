/** @type {import('tailwindcss').Config} */
export default {
  content: [
    './components/**/*.{js,vue,ts}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './app.vue',
  ],
  theme: {
    extend: {
      fontFamily: {
        cream: ['Cream'],
      },
      colors: {
        'las-green': '#4db276',
        'las-orange': '#e58252',
        'las-pink': '#e88ab7',
        // Dark mode colors
        'las-d-prune': '#7b6771',
        'las-d-brown': '#675f63',
        'las-d-green': '#69ae85',
        'las-d-orange': '#de9674',
        'las-d-pink': '#d89bb9',
      },
    },
  },
  darkMode: ['selector', '.dark-mode'],
  plugins: [],
};
